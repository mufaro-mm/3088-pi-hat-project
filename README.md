# 3088 pi HAT project 

For this project we will be designing a sense microHat that will sense and monitor temperature readings. The Hat will be attached to a predesigned sensor that will measure the temperature as a function of voltage and this will be converted to degrees

To use this hat, it has to be connected to a power supply and an LED will turn on to indicate the device is on. the hat will them sense temperature and using an analogue to digital convertor this will be changed to a voltage reading which will then be amplified using an operational amplifier and depending on the value of the voltage, a certain colour LED will emit light to nofity the user of the temperature range. 
